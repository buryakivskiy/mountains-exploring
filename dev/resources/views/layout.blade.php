<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    <title>Головна сторінка</title>
</head>

<body>
    @yield('main_content')
    <footer>
        <div class="footer_div">
            <a href="/" class="footer_logo">
                <img src="{{asset('images/logo.png')}}" alt="mountains exploring">
            </a>
        </div>
        <ul class="mnu_bottom">
            <li>@2022 Буряківський Сергій | веб розробка</li>
            <li>Копіювання матеріалів без згоди автора заборонене.</li>
        </ul>
        <div class="social">
            <a href="https://www.youtube.com/c/propohody"><img src="{{asset('images/yt.png')}}" alt="youtube"></a>
            <a href="#"><img src="{{asset('images/in.png')}}" alt="instagram"></a>
            <a href="#"><img src="{{asset('images/fb.png')}}" alt="facebook"></a>
            <a href="#"><img src="{{asset('images/tg.png')}}" alt="telegram"></a>
            <a href="#"><img src="{{asset('images/em.png')}}" alt="email"></a>
        </div>
    </footer>
</body>

</html>