@extends('layout')

@section('main_content')
    <header>
        <a href="/" class="header_logo">
            <img src="{{asset('images/logo.png')}}" alt="mountains exploring">
        </a>
        <ul class="mnu_top">
            <li style="border-color: #E45F4D;"><a href="/equipment" style="color: #E45F4D;">спорядження</a></li>
            <li><a href="/tours">маршрути</a></li>
            <li><a href="http://gryada.com.ua/forum/viewforum.php?f=39">форум</a></li>
            <li><a href="/contactUs">звʼязок з нами</a></li>
        </ul>
    </header>
    <div class="main">
        <div class="search">
            <div class="page_name">
                <h5>{{ $category }}</h5>
            </div>
            <div class="search_input">
                <input type="name" class="search_field" placeholder="Введіть щось для пошуку...">
                <img src="{{asset('images/search.jpg')}}" alt="">
            </div>
        </div>
        <div class="products_list">
            @foreach($products as $product)
                <a href="/overview/{{$product->id}}">
                    <div class="product">
                        <img src="{{asset($product->image)}}" alt="">
                        <div class="product_content">
                            <h5>{{ $product->name }}</h5>
                            <div>
                                <h6>{{ $product->price }}</h6>
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach()
        </div>
    </div>
@endsection()