@extends('layout')

@section('main_content')
<header>
    <a href="/" class="header_logo">
        <img src="{{asset('images/logo.png')}}" alt="mountains exploring">
    </a>
    <ul class="mnu_top">
        <li><a href="/equipment">спорядження</a></li>
        <li style="border-color: #E45F4D;"><a style="color: #E45F4D;" href="/tours">маршрути</a></li>
        <li><a href="http://gryada.com.ua/forum/viewforum.php?f=39">форум</a></li>
        <li><a href="/contactUs">звʼязок з нами</a></li>
    </ul>
</header>
<div class="main">
    <div class="search">
        <div class="page_name">
            <h5>маршрути</h5>
        </div>
        <div class="search_input">
            <input type="name" class="search_field" placeholder="Введіть щось для пошуку...">
            <img src="{{asset('images/search.jpg')}}" alt="">
        </div>
    </div>
    <div class="catalog">
        @foreach($tours as $tour)
        <a href="/tour/{{$tour->id}}">
            <div class="hike">
                <img src="{{asset($tour->image)}}" alt="" class="hike_photo">
                <div class="hike_text">
                    <h6 class="author">{{ $tour->author }}</h6>
                    <h6 class="hike_name">{{ $tour->name }}</h6>
                    <h6 class="hike_desc">{{ $tour->info }}</h6>
                </div>
            </div>
        </a>
        @endforeach()
    </div>
</div>
@endsection()