@extends('layout')

@section('main_content')
    <header>
        <a href="/" class="header_logo">
            <img src="{{asset('images/logo.png')}}" alt="mountains exploring">
        </a>
        <ul class="mnu_top">
            <li style="border-color: #E45F4D;"><a href="/equipment" style="color: #E45F4D;">спорядження</a></li>
            <li><a href="/tours">маршрути</a></li>
            <li><a href="http://gryada.com.ua/forum/viewforum.php?f=39">форум</a></li>
            <li><a href="/contactUs">звʼязок з нами</a></li>
        </ul>
    </header>
    <div class="main">
        <div class="catalog">
            <a href="/products/tent">
                <div class="catalog_item">
                    <img src="{{ asset('images/tent.jpeg') }}" alt="">
                    <div class="item_content">
                        <h5>намети</h5>
                    </div>
                </div>
            </a>
            <a href="/products/bag">
                <div class="catalog_item">
                    <img src="{{ asset('images/bags.jpeg') }}" alt="">
                    <div class="item_content">
                        <h5>рюкзаки</h5>
                    </div>
                </div>
            </a>
            <a href="/products/sleeping_bag">
                <div class="catalog_item">
                    <img src="{{ asset('images/sleeping_bag.jpeg') }}" alt="">
                    <div class="item_content">
                        <h5>спальні мішки</h5>
                    </div>
                </div>
            </a>
            <a href="/products/rugs">
                <div class="catalog_item">
                    <img src="{{ asset('images/rugs.jpeg') }}" alt="">
                    <div class="item_content">
                        <h5>килимки</h5>
                    </div>
                </div>
            </a>
            <a href="/products/food">
                <div class="catalog_item">
                    <img src="{{ asset('images/food.jpeg') }}" alt="">
                    <div class="item_content">
                        <h5>їжа</h5>
                    </div>
                </div>
            </a>
            <a href="/products/lights">
                <div class="catalog_item">
                    <img src="{{ asset('images/lights.jpeg') }}" alt="">
                    <div class="item_content">
                        <h5>ліхтарики</h5>
                    </div>
                </div>
            </a>
            <a href="/products/knifes">
                <div class="catalog_item">
                    <img src="{{ asset('images/knifes.jpeg') }}" alt="">
                    <div class="item_content">
                        <h5>ножі</h5>
                    </div>
                </div>
            </a>
            <a href="/products/burners">
                <div class="catalog_item">
                    <img src="{{ asset('images/burners.jpeg') }}" alt="">
                    <div class="item_content">
                        <h5>пальники</h5>
                    </div>
                </div>
            </a>
            <a href="/products/utensils">
                <div class="catalog_item">
                    <img src="{{ asset('images/utensils.jpeg') }}" alt="">
                    <div class="item_content">
                        <h5>посуд</h5>
                    </div>
                </div>
            </a>
            <a href="/products/poles">
                <div class="catalog_item">
                    <img src="{{ asset('images/poles.jpeg') }}" alt="">
                    <div class="item_content">
                        <h5>трекінгові палиці</h5>
                    </div>
                </div>
            </a>
            <a href="/products/accessories">
                <div class="catalog_item">
                    <img src="{{ asset('images/accessories.jpeg') }}" alt="">
                    <div class="item_content">
                        <h5>аксесуари</h5>
                    </div>
                </div>
            </a>
            <a href="/products/literature">
                <div class="catalog_item">
                    <img src="{{ asset('images/literature.jpeg') }}" alt="">
                    <div class="item_content">
                        <h5>література</h5>
                    </div>
                </div>
            </a>
        </div>
    </div>
@endsection()