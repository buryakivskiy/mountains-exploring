@extends('layout')

@section('main_content')
    <header>
        <a href="/" class="header_logo">
            <img src="{{asset('images/logo.png')}}" alt="mountains exploring">
        </a>
        <ul class="mnu_top">
            <li style="border-color: #E45F4D;"><a href="/equipment" style="color: #E45F4D;">спорядження</a></li>
            <li><a href="/tours">маршрути</a></li>
            <li><a href="http://gryada.com.ua/forum/viewforum.php?f=39">форум</a></li>
            <li><a href="/contactUs">звʼязок з нами</a></li>
        </ul>
    </header>
    <div class="main">
        <div class="search">
            <div class="page_name">
                <h5>{{$product->category}}</h5>
            </div>
            <div class="search_input">
                <input type="name" class="search_field" placeholder="Введіть щось для пошуку...">
                <img src="{{asset('images/search.jpg')}}" alt="">
            </div>
        </div>
        <div class="general_info">
            <div class="image">
                <img src="{{asset($product->image)}}" alt="">
            </div>
            <div class="checkout">
                <div class="price_holder">
                    <h5>{{ $product->name }}</h5>
                    <div>
                        <div class="price">
                            <a href="/contactUs">
                                <div>
                                    <h6>{{ $product->price }}</h6>
                                </div>
                            </a>
                        </div>
                        <img src="{{asset($product->logo)}}" alt="" class="product_logo">
                    </div>
                </div>
                <div class="price_holder">
                    <div class="order">
                        <div class="price">
                            <a href="/contactUs">
                                <div>
                                    <h6>Купити</h6>
                                </div>
                            </a>
                        </div>
                        <div class="price">
                            <a href="/contactUs">
                                <div>
                                    <h6>Взяти в прокат</h6>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="description">
            <p style="text-align:center; font-size: 24px"><strong>Опис</strong><br></p>
            <h6>
                {!! $product->description !!}
            </h6>
        </div>
    </div>
@endsection()