@extends('layout')

@section('main_content')
<header>
    <a href="/" class="header_logo">
        <img src="{{asset('images/logo.png')}}" alt="mountains exploring">
    </a>
    <ul class="mnu_top">
        <li><a href="/equipment">спорядження</a></li>
        <li><a href="/tours">маршрути</a></li>
        <li><a href="http://gryada.com.ua/forum/viewforum.php?f=39">форум</a></li>
        <li><a href="/contactUs">звʼязок з нами</a></li>
    </ul>
</header>
<div class="main">
    <div class="slideshow">
        <div class="slideshow-item">
            <img src="{{asset('images/slide1.jpg')}}" alt="">
            <div class="slideshow-item-text">
                <h5>Як вибрати якicне спорядження:</h5>
                <p>поради від туристів</p>
            </div>
        </div>
        <div class="slideshow-item">
            <img src="{{asset('images/slide2.jpg')}}" alt="">
            <div class="slideshow-item-text">
                <h5>Найкращі піші маршрути</h5>
                <p>+ відео з ними </p>
            </div>
        </div>
        <div class="slideshow-item">
            <img src="{{asset('images/slide3.jpg')}}" alt="">
            <div class="slideshow-item-text">
                <h5>Блоги про походи</h5>
                <p>від досвідчених туристів</p>
            </div>
        </div>
        <div class="slideshow-item">
            <img src="{{asset('images/slide4.jpg')}}" alt="">
            <div class="slideshow-item-text">
                <h5>Вирушай у похід з нами!</h5>
                <p>звертайся за консультацією</p>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="contact_us">
                <img src="{{asset('images/slide1.jpg')}}" alt="">
                <div class="input">
                    <div class="text">
                        <h6>Ми вас проконсультуємо:</h6>
                    </div>
                    <div class="form">
                        <div class="form_name">
                        </div>
                        <div class="form_name">
                        </div>
                        <h4>></h4>
                    </div>
                </div>
            </div>
            <a href="/tour/1" style="margin-top: 150px;">
                <div class="hike">
                    <div class="hike_photo">
                        <h6>3-5 днів;<br>54,8-73,9 км</h6>
                    </div>
                    <div class="hike_text">
                        <h6 class="author">Христина Присяжна</h6>
                        <h6 class="hike_name">Мармароси – Чорногора.</h6>
                        <h6 class="hike_desc">Мармароси – це любов з першого погляду. Масив виявився зовсім не великим у масштабах Українських Карпат. Але настільки іншим, що сюди хочеться повертатись знову і знову</h6>
                    </div>
                </div>
            </a>
            <div class="user2">
                <img src="{{asset('images/user1.jpeg')}}" alt="">
                <h5>Антон Прохоров</h5>
                <p>Засновник блогу ПроПоходи. Свої перші гірські кроки зробив 27 років тому в Алтайських горах. Зараз живе і працює в Києві. Обожнює гори, рух, складні задачі і легке спорядження.</p>
            </div>
        </div>
        <div class="row">
            <div class="user">
                <img src="{{asset('images/user1.jpeg')}}" alt="">
                <h5>Антон Прохоров</h5>
                <p>Засновник блогу ПроПоходи. Свої перші гірські кроки зробив 27 років тому в Алтайських горах. Зараз живе і працює в Києві. Обожнює гори, рух, складні задачі і легке спорядження.</p>
            </div>
            <a href="/tour/1">
                <div class="hike">
                    <div class="hike_photo">
                        <h6>3-5 днів;<br>54,8-73,9 км</h6>
                    </div>
                    <div class="hike_text">
                        <h6 class="author">Христина Присяжна</h6>
                        <h6 class="hike_name">Мармароси – Чорногора.</h6>
                        <h6 class="hike_desc">Мармароси – це любов з першого погляду. Масив виявився зовсім не великим у масштабах Українських Карпат. Але настільки іншим, що сюди хочеться повертатись знову і знову</h6>
                    </div>
                </div>
            </a>
            <a href="/tour/1">
                <div class="hike">
                    <div class="hike_photo">
                        <h6>3-5 днів;<br>54,8-73,9 км</h6>
                    </div>
                    <div class="hike_text">
                        <h6 class="author">Христина Присяжна</h6>
                        <h6 class="hike_name">Мармароси – Чорногора.</h6>
                        <h6 class="hike_desc">Мармароси – це любов з першого погляду. Масив виявився зовсім не великим у масштабах Українських Карпат. Але настільки іншим, що сюди хочеться повертатись знову і знову</h6>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="references">
        <div class="camping">
        </div>
        <div class="contact">
            <h6 style="color: #E45F4D;">ми</h6>
            <h6>допоможемо</h6>
            <h6 style="color: #E45F4D;">ВАМ</h6>
            <h6>скласти</h6>
            <h6 style="color: #E45F4D;">власний</h6>
            <h6>маршрут</h6>
            <img src="{{ asset('images/place.jpg')}}" alt="">
            <a href="/contactUs" style="text-decoration: none;">
                <div class="button">
                    <h6>Замовити</h6>
                </div>
            </a>
        </div>
        <div class="rafting">
        </div>
    </div>
</div>
@endsection()