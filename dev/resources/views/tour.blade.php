@extends('layout')

@section('main_content')
    <header>
        <a href="/" class="header_logo">
            <img src="{{asset('images/logo.png')}}" alt="mountains exploring">
        </a>
        <ul class="mnu_top">
            <li><a href="/equipment">спорядження</a></li>
            <li style="border-color: #E45F4D;"><a style="color: #E45F4D;" href="/tours">маршрути</a></li>
            <li><a href="http://gryada.com.ua/forum/viewforum.php?f=39">форум</a></li>
            <li><a href="/contactUs">звʼязок з нами</a></li>
        </ul>
    </header>
    <div class="main">
        <div class="search">
            <div class="page_name">
                <h5>маршрути</h5>
            </div>
            <div class="search_input">
                <input type="name" class="search_field" placeholder="Введіть щось для пошуку...">
                <img src="{{asset('images/search.jpg')}}" alt="">
            </div>
        </div>
        <img class="tour_img" src="{{asset($tour->intro)}}" alt="">
        <div class="tour_general">
            <div class="general_text">
                <p>{!! $tour->general !!}</p>
            </div>
            <div class="general_author">
                <div class="author_block">
                    <img src="{{asset($tour->author_image)}}" alt="">
                    <h5>{{ $tour->author }}</h5>
                    <p>{!! $tour->author_description !!}</p>
                </div>
            </div>
        </div>
        <div class="video">
            <iframe width="800" height="452.5" src="{{$tour->video}}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="description">
            <p style="text-align:center; font-size: 24px"><strong>Опис маршруту:</strong><br></p>
            <h6>
                {!! $tour->description !!}
            </h6>
        </div>
    </div>
@endsection()