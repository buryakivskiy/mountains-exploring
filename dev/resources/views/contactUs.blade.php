@extends('layout')

@section('main_content')
    <header>
        <a href="/" class="header_logo">
            <img src="{{asset('images/logo.png')}}" alt="mountains exploring">
        </a>
        <ul class="mnu_top">
            <li><a href="/equipment">спорядження</a></li>
            <li><a href="/tours">маршрути</a></li>
            <li><a href="http://gryada.com.ua/forum/viewforum.php?f=39">форум</a></li>
            <li style="border-color: #E45F4D;"><a href="/contactUs" style="color: #E45F4D;">звʼязок з нами</a></li>
        </ul>
    </header>
    <div class="main">
        <div class="search">
            <div class="page_name">
                <h5>звʼязок з нами</h5>
            </div>
        </div>
        <div class="contact_body">
            <div class="static_info">
                <div class="adress">
                    <img src="{{asset('images/adress.jpg')}}" alt="">
                    <div>
                        <h4>Наша адреса</h4>
                        <h5>м.Хмельницький, вул. Купріна 66\1</h5>
                    </div>
                </div>
                <div class="phone">
                    <img src="{{asset('images/phone.jpg')}}" alt="">
                    <div>
                        <h4>Номер телефону</h4>
                        <h5>+38(096)8234567</h5>
                    </div>
                </div>
                <div class="email">
                    <img src="{{asset('images/email.jpg')}}" alt="">
                    <div>
                        <h4>Електронна адреса</h4>
                        <h5>buryakivskiy21@gmail.com</h5>
                    </div>
                </div>
            </div>
            <div class="input_form">
                <h2>Відправити повідомлення</h2>
                @if($errors->any())
                    <div class="error">
                        <h6>*не відправленно, перевірте данні та спробуйте ще раз</h6>
                    </div>
                @endif
                <form method="post" action="/contactUs/send">
                    @csrf
                    <input type="name" name="name" id="name" class="field" placeholder="Як до вас звертатись?">
                    <input type="email" name="email" id="email" class="field" placeholder="Вкажіть вашу електронну адресу...">
                    <input type="text" name="message" id="message" class="field" style="height: 80px;" placeholder="Введіть ваше повідомлення...">
                    <button type="submit" class="send">
                        <h3>Відправити</h3>
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection()