<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function send(Request $request){
        $valid = $request->validate([
            'name' => 'required|max:100',
            'email' => 'required|min:10|max:100',
            'message' => 'required|min:10|max:1000'
        ]);

        $message = new Message();
        $message->name = $request->input('name');
        $message->email = $request->input('email');
        $message->text = $request->input('message');

        $message->save();

        return redirect()->route('contactUs');
    }
}
