<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Tour;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function main()
    {
        return view('main');
    }

    public function equipment()
    {
        return view('equipment');
    }

    public function products($category)
    {
        $products = Product::where('category', $category);
        return view('products', [
            'category' => $category, 
            'products' => $products->get()
        ]);
    }

    public function overview($productId)
    {
        $product = Product::find($productId);
        return view('overview', [
            'category' => $product->category, 
            'product' => $product
        ]);
    }

    public function contactUs()
    {
        return view('contactUs');
    }

    public function tours()
    {
        $tours = Tour::get();
        return view('tours', [
            'tours' => $tours
        ]);
    }

    public function tour($tourId)
    {
        $tour = Tour::find($tourId);
        return view('tour', [ 
            'tour' => $tour
        ]);
    }
}
