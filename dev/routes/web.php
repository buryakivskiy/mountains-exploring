<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\ContactUsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainController::class, 'main']);

Route::get('/equipment', [MainController::class, 'equipment']);

Route::get('/products/{category}', [MainController::class, 'products']);

Route::get('/overview/{productId}', [MainController::class, 'overview']);

Route::get('/contactUs', [MainController::class, 'contactUs'])->name('contactUs');

Route::get('/tours', [MainController::class, 'tours'])->name('tours');

Route::get('/tour/{tourId}', [MainController::class, 'tour'])->name('tour');

Route::post('/contactUs/send', [ContactUsController::class, 'send']);

